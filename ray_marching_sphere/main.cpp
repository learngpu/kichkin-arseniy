//
// Created by arseniy on 21.09.23.
//
#include <utils/camera.h>
#include <fstream>
#include <ranges>
#include "iostream"


f32 t_max = 10.f;
f32 epsilon = 1e-6f;
f32 R = 1.f;
auto [Rx, Ry, Rz] = vec3{0.f, 0.f, 0.f};
vec3 const albedo = {0.8f, 0.3f, 0.3f};

struct Hit {
    vec3 r;
};

f32 SDF(vec3 point) {

    f32 distance = /*std::sqrt(std::pow(std::max(point.x, 0.f), 2.f) + std::pow(std::max(point.y, 0.f), 2.f) +
                               std::pow(std::max(point.z, 0.f), 2.f)) +
                     std::min(std::max(point.x, std::max(point.y, point.z)), 0.f);*/
            std::sqrt((point.x - Rx) * (point.x - Rx) + (point.y - Ry) * (point.y - Ry) +
                      (point.z - Rz) * (point.z - Rz)) - R;
    return distance;
}

auto closestHit = [](Ray const ray, f32 dt = 0.f) -> std::optional<Hit> {
    std::optional<Hit> hit;
    vec3 pos = ray.origin + ray.direction * dt;
    //vec3 q = {0.f, 0.f, 0.f};
    do {
        f32 distance = SDF(pos);
        if (std::fabs(distance) < epsilon) {
            hit = Hit(pos);
            return hit;
        }
        if (distance < 0.f) {
            dt -= distance;
        } else { dt += distance; }

        pos = ray.origin + ray.direction * dt;
    } while (dt <= t_max);
    //q = {std::abs(pos.x) - Rx, std::abs(pos.y) - Ry, std::abs(pos.z) - Rz};
    return hit;

};


auto const trace = [](Ray const ray) noexcept
        -> vec3 {
    vec3 const skyColor = {0.53f, 0.81f, 0.92f};
    vec3 const lightColor = {1.00f, 0.98f, 0.88f};
    vec3 const lightDir = normalize({3.f, 3.f, -1.f});
    auto const hit = closestHit(ray);
    if (!hit)
        return skyColor;
    auto const r = hit->r;
    vec3 const norm = normalize(/*{SDF({r.x + epsilon, r.y, r.z}) - SDF({r.x - epsilon, r.y, r.z}),
                                 SDF({r.x, r.y + epsilon, r.z}) - SDF({r.x, r.y - epsilon, r.z}),
                                 SDF({r.x, r.y, r.z + epsilon}) - SDF({r.x, r.y, r.z - epsilon})}*/
            r - vec3{Rx, Ry, Rz});

    f32 const NL = std::max(0.f, dot(norm, lightDir));

    auto const shadowHit = closestHit({r, lightDir}, 2e-4f);
    return albedo * (skyColor * 0.1f + lightColor * NL * (shadowHit ? 0.f : 0.7f));


};

int main() {


    std::ofstream file("out.ppm");
    u32 const width = 1920u;
    u32 const height = 1080u;
    file << "P3\n" << width << ' ' << height << "\n255\n";

    Camera const camera =
            {
                    .origin = {0.f, 0.f, -2.f},
                    .at = {0.f, 0.f, 0.f},
                    .up = {0.f, 1.f, 0.f},
                    .fov = 0.55f,
                    .aspectRatio = f32(width) / f32(height),
            };
    for (u32 y = 0u; y < height; ++y)
        for (u32 x = 0u; x < width; ++x) {
            f32 const u = -1.f + 2.f * (0.5f + f32(x)) / f32(width);
            f32 const v = 1.f - 2.f * (0.5f + f32(y)) / f32(height);
            auto const [r, g, b] = trace(camera.castRay(u, v));

            auto const encode = +[](f32 const f) noexcept {
                f32 const c = f < 0.0031308f
                              ? f * 12.92f
                              : 1.055f * std::pow(f, 1.f / 2.4f) - 0.055f;
                return u32(std::round(255.f * c));
            };
            file << encode(r) << ' '
                 << encode(g) << ' '
                 << encode(b) << ' ';
        }


}

